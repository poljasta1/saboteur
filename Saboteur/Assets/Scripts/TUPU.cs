﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TUPU : MonoBehaviour
{
    public GameObject[,] tablero = new GameObject[10,5];
    public GameObject huecoPrefab;
    public GameObject huecos;


    //Selected
    public bool manoSelected = false;
    public bool tableroSelected = false;

    public int xMano,yMano ;
    public int xTable, yTable;

    public GameObject[,] mano = new GameObject[2, 6];


    public GameObject cartaTablero;

    // Start is called before the first frame update
    void Start()
    {
        try
        {
            GenerarTablero();

        }
        catch(InvalidCastException e)
        {
            Console.WriteLine("Excepcion 0: {0}", e.Source);

        }
    }

    public void GenerarTablero()
    {
        for (int x = 0; x < 10; x++)
        {
            for (int y = 0; y < 5; y++)
            {
                float fx = x * 1.1f;
                float fy = y * 2.1f;
                GameObject espacio = Instantiate(huecoPrefab, new Vector3(-fx ,-fy , 0), Quaternion.identity);
                espacio.name =  x + "." + y ;
                espacio.transform.SetParent(huecos.transform);
                tablero[x, y] = espacio;
                
               
            }
        }
        Debug.Log(tablero);
    }
    



    // Update is called once per frame
    void Update()
    {

        if(manoSelected && tableroSelected){

            mano[xMano,yMano].transform.position = new Vector3(tablero[xTable,yTable].transform.position.x,tablero[xTable,yTable].transform.position.y,0);

            Destroy(tablero[xTable,yTable]);


            manoSelected = false;
            tableroSelected = false;




        }




    }
}
