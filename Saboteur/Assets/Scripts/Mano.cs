﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mano : MonoBehaviour
{
    public GameObject[,] tablero2 = new GameObject[2,3];
    public GameObject huecoPrefab2;
    public GameObject mano;
    private TUPU tupu;

    // Start is called before the first frame update
    void Start()
    {
        tupu = FindObjectOfType<TUPU>();
        GenerarMano();

    }
       

    public void GenerarMano()
    {
        for (int x = 0; x < 2; x++)
        {
            for (int y = 0; y < 3; y++)
            {
                //float fx = x * 1.1f;
                //float fy = y * 2.1f;
                GameObject espacio = Instantiate(huecoPrefab2, new Vector3((this.transform.position.x + -x * 1.1f) ,(this.transform.position.y + -y * 2.1f) , 0), Quaternion.identity);
                espacio.name =  x + "." + y ;
                espacio.transform.SetParent(mano.transform);
                tablero2[x, y] = espacio;
               
            }
        }
        tupu.mano = tablero2;
      
    }




    // Update is called once per frame
    void Update()
    {
        
    }
}
