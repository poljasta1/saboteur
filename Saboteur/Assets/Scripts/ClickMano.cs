﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickMano : MonoBehaviour
{
    char[] spearator = {'.'};
    private TUPU tupu;

    // Start is called before the first frame update
    void Start()
    {
        tupu = FindObjectOfType<TUPU>();

    }
    
    private void OnMouseDown()
    {
       
        string name = this.name;
        string[] strlist = name.Split(spearator);
        print(strlist[0]+ "."+ strlist[1]);
        tupu.xMano = System.Convert.ToInt32(strlist[0]);
        tupu.yMano = System.Convert.ToInt32(strlist[1]);
        tupu.manoSelected = true;

    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
